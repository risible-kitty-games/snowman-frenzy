extends Area2D

var speed = 200

@onready var notifier = $Notifier
@onready var animation = $AnimationPlayer

func _physics_process(delta):
	global_position.x += speed*delta
	if (speed == 200):
		animation.play("idle")
	else:
		animation.play("poof")

func _on_notifier_screen_exited():
	queue_free()

func _on_area_entered(area):
	speed=40
	area.enemy_die()
	await get_tree().create_timer(.4).timeout
	queue_free()

func _on_timer_timeout():
	speed=40
	await get_tree().create_timer(.4).timeout
	queue_free()

