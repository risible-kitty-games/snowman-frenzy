extends Node2D

func play(sfx = null):
	if sfx:
		get_node(sfx).play()

func stop(sfx = null):
	if sfx:
		get_node(sfx).stop()
