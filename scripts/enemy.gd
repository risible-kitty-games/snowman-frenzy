extends Area2D

signal enemy_died

@onready var animation = $AnimationPlayer


@export var speed = 100
@export var enemy_alive = true

func _physics_process(delta):
	global_position.x += -speed*delta
	if (enemy_alive == true):
		animation.play("idle")
	else:
		enemy_melt()
	
func enemy_die():
	emit_signal("enemy_died")
	enemy_alive = false
	await get_tree().create_timer(.5).timeout
	queue_free()


func _on_body_entered(body):
	body.take_damage()
	enemy_alive = false
	await get_tree().create_timer(.5).timeout
	queue_free()

func enemy_melt():
	speed = 1
	animation.play("melt")
