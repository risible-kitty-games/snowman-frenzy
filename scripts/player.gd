extends CharacterBody2D

signal took_damage
signal player_died

var speed = 200

var hot_air_scene = preload("res://scenes/hot_air.tscn")

@onready var hot_air_container = $HotAirContainer
@onready var animation = $AnimationPlayer
@onready var sfx = $sfx

func _process(delta):
	if Input.is_action_just_pressed("shoot"):
		shoot()
		
func get_input():
	var input_direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	velocity = input_direction * speed

func _physics_process(delta):
	get_input()
	move_and_slide()
	if (velocity.x != 0) or (velocity.y != 0):
		animation.play("walk")
		if $Timer.time_left <=0:
			sfx.play("walk")
			$Timer.start(0.7)
	else:
		animation.stop()
		sfx.stop("walk")
	
	var screen_size = get_viewport_rect().size
	global_position = global_position.clamp(Vector2(0,0), screen_size)
	
	
	
func shoot():
	sfx.play("blowdryer")
	var hot_air_instance = hot_air_scene.instantiate()
	hot_air_container.add_child(hot_air_instance)
	hot_air_instance.global_position = global_position
	hot_air_instance.global_position.x += 40
	
func take_damage():
	print("ouch")
	emit_signal("took_damage")
	
func die():
	queue_free()
	emit_signal("player_died")
